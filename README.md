# README

* Ruby version

    2.5.x

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

## Development

```
$ docker-compose run --rm rails bundle install
$ docker-compose run --rm rails yarn install
$ docker-compose run --rm webpacker yarn install
$ docker-compose up
```

Access to `http://localhost:3000`
