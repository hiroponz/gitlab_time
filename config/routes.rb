Rails.application.routes.draw do
  get 'login', to: 'sessions#new', as: 'login'
  post 'logout', to: 'sessions#destroy', as: 'logout'
  get 'callback', to: 'sessions#create', as: 'callback'
end
