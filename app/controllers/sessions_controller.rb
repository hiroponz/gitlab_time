class SessionsController < ApplicationController
  def new
    @url = client.auth_code.authorize_url(redirect_uri: callback_url, state: SecureRandom.hex)
  end

  def create
    token = client.auth_code.get_token(params[:code], redirect_uri: callback_url, state: params[:state])
    puts '*'*100
    puts token.to_hash
  end

  def destroy
  end

  private

  def client
    @client ||= OAuth2::Client.new(
      Settings.oauth2.application_id,
      Settings.oauth2.secret,
      site: Settings.oauth2.url
    )
  end
end
